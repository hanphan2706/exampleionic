import { Component}                         from '@angular/core';
import { NavController, AlertController }   from 'ionic-angular';

import { IBookInfo }                        from '../../app/shared/models/interfaces';
import { DataService }                      from '../../app/shared/services/data/data.service';
import { AuthService }                      from '../../app/shared/services/auth/auth.service';

@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html'
})
export class CartPage{
  books: IBookInfo[] = [];

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, private _dataService: DataService, private _authService: AuthService) {
  }

  getCart() {
    if (this._authService.isLoggedIn()) {
      this._dataService.checkCart().then((res) => {
        if (res) {
          this.books = res;
        }
      })
    } else {
      this.books = [];
    }
  }

  ionViewDidEnter(){
    this.getCart();
  }

  remove(book) {
    let prompt = this.alertCtrl.create({
          title: 'No login',
          message: 'Do you want to remove this book?',
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
                console.log('Cancel clicked');
              }
            },
            {
              text: 'OK',
              handler: () => {
                this._dataService.removeFromCart(book.id).then((res) => {
                  book.isAdded = false;
                  this.getCart();
                })
              }
            }
          ]
      });
      prompt.present();
  }
}