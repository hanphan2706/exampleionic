import { Component }                      from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { AuthService }                    from '../../app/shared/services/auth/auth.service';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {
  currentUser: any = {};
  isLoggedIn: boolean = false;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, private _authService: AuthService) {
    this.isLoggedIn = _authService.isLoggedIn();
    if (this.isLoggedIn) 
      this.currentUser = _authService.userDetails;
  }

  login() {
    this._authService.signInWithFacebook().then((res) => {
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
    });;
  }

  logout() {
    let prompt = this.alertCtrl.create({
        title: 'Confirm logout',
        message: 'Do you want to logout?',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Ok',
            handler: () => {
              this._authService.logout().then((res) => {
                this.navCtrl.setRoot(this.navCtrl.getActive().component);
              });
            }
          }
        ]
    });
    prompt.present();
  }
}