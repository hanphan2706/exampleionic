import { Component}                       from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { AngularFireAuth }                from 'angularfire2/auth';

import { IBookInfo }                      from '../../app/shared/models/interfaces';
import { DataService }                    from '../../app/shared/services/data/data.service';
import { AuthService }                    from '../../app/shared/services/auth/auth.service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  books: IBookInfo[] = [];

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, _firebaseAuth: AngularFireAuth, private _dataService: DataService, private _authService: AuthService) {
    const authObserver = _firebaseAuth.authState.subscribe( user => {
      this.getBooks();
      authObserver.unsubscribe();
    });
  }

  getBooks() {
    if (this._authService.isLoggedIn()) {
      this._dataService.getBooks().then(res => {
        this.books = res;
      })
    } else {
      this.books = this._dataService.createBooks();
    }
  }

  ionViewDidEnter() {
    this.getBooks();
  }

  buy(book) {
    if (this._authService.isLoggedIn()) {
      let prompt = this.alertCtrl.create({
          title: 'Confirm purchase',
          message: 'Do you want to buy this book?',
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
                console.log('Cancel clicked');
              }
            },
            {
              text: 'Buy',
              handler: () => {
                this.addToCart(book);
              }
            }
          ]
      });
      prompt.present();
    } else {
      let prompt = this.alertCtrl.create({
          title: 'No login',
          message: 'Do you want to login with facebook?',
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
                console.log('Cancel clicked');
              }
            },
            {
              text: 'OK',
              handler: () => {
                this._authService.signInWithFacebook().then(res => {
                  this.addToCart(book);
                  this.navCtrl.setRoot(this.navCtrl.getActive().component);
                });
              }
            }
          ]
      });
      prompt.present();
    }
  }

  addToCart(book) {
    book.isAdded = true;
    this._dataService.addToCart(book);
  }
}