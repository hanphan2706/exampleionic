import { NgModule, ErrorHandler }                   from '@angular/core';
import { BrowserModule }                            from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { IonicStorageModule }                       from '@ionic/storage';
import { Environment }                              from './environments/environments';
import { AngularFireModule }                        from 'angularfire2';
import { AngularFireDatabaseModule }                from 'angularfire2/database';
import { AngularFireAuthModule }                    from 'angularfire2/auth';

import { StatusBar }                                from '@ionic-native/status-bar';
import { SplashScreen }                             from '@ionic-native/splash-screen';
import { Facebook }                                 from '@ionic-native/facebook';
import { BookItemModule }                           from './shared/components/bookitem/bookitem.module';
import { MyApp }                                    from './app.component';

import { AuthService }                              from './shared/services/auth/auth.service';
import { DataService }                              from './shared/services/data/data.service';


import { CartPage }                                 from '../pages/cart/cart';
import { ProfilePage }                              from '../pages/profile/profile';
import { HomePage }                                 from '../pages/home/home';
import { TabsPage }                                 from '../pages/tabs/tabs';

@NgModule({
  declarations: [
    MyApp,
    CartPage,
    ProfilePage,
    HomePage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    BookItemModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(Environment.firebase, 'angular-auth-firebase'),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    CartPage,
    ProfilePage,
    HomePage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Facebook,
    AuthService,
    DataService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}