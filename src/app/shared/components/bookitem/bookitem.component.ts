import { Component, Input }               from '@angular/core';

import { AuthService }                    from '../../services/auth/auth.service';

@Component({
  selector: 'book-item',
  templateUrl: 'bookitem.component.html'
})
export class BookItemComponent{
  @Input() book: any = {};
  @Input() type: string = 'card';
  @Input() addToCartFunc = Function;
  @Input() removeFromCartFunc = Function;

  constructor() {}

  buy(book) {
    this.addToCartFunc(book);
  }

  remove(book) {
    this.removeFromCartFunc(book);
  }
}