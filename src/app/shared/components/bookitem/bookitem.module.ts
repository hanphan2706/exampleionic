import { NgModule }          from '@angular/core';
import { BrowserModule }                            from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { IonicStorageModule }                       from '@ionic/storage';

import { BookItemComponent } from './bookitem.component'

@NgModule({
  declarations: [
    BookItemComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(BookItemComponent),
    IonicStorageModule.forRoot()
  ],
  exports: [ BookItemComponent ]
})
export class BookItemModule { }