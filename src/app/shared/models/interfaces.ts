export interface IBookInfo {
    id: number;
    title: string;
    description: string;
    imgUrl: string;
    isAdded: boolean;
}