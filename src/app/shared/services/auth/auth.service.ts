import { Injectable }       from '@angular/core';
import { Observable }       from 'rxjs/Observable';
import { AngularFireAuth }  from 'angularfire2/auth';
import * as firebase        from 'firebase/app';

@Injectable()
export class AuthService {

  private user: Observable<firebase.User>;
  userDetails: firebase.User = null;
  
  constructor(private _firebaseAuth: AngularFireAuth) { 
      this.user = _firebaseAuth.authState;
      this.user.subscribe(
          (user) => {
            if (user) {
              this.userDetails = user;
              console.log(this.userDetails);
            }
            else {
              this.userDetails = null;
            }
          }
        );
    }

    
  signInWithFacebook() {
    return this._firebaseAuth.auth.signInWithPopup(
      new firebase.auth.FacebookAuthProvider()
    )
  }
   
  isLoggedIn() {
    if (this.userDetails == null ) {
        return false;
      } else {
        return true;
      }
  }

  logout() {
    return this._firebaseAuth.auth.signOut()
      
    }
  }