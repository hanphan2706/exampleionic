import { Injectable }   from "@angular/core";
import { Storage }      from '@ionic/storage';
import { IBookInfo }    from '../../models/interfaces';
import { AuthService }  from '../auth/auth.service';

@Injectable()
export class DataService {
  userId: string;
  booksInCart: string = 'booksInCart';
  listBooks: string = 'listBooks';

  constructor(public _storage: Storage, private _authService: AuthService) {
    if (_authService.isLoggedIn()) 
      this.userId = _authService.userDetails.uid;
      this.booksInCart += this.userId;
      this.listBooks += this.userId;
  }

  checkCart() {
    return this._storage.get(this.booksInCart);
  }

  addToCart(book) {
    this.isAdded(book.id).then(res => {
      if (!res) {
        this.getBooks().then(result => {
          var itemFound = result.filter(x => x.id == book.id)[0];
          itemFound.isAdded = true;
          this._storage.set(this.listBooks, result);
        })

        return this.checkCart().then(result => {
          if (result) {
            result.push(book);
            return this._storage.set(this.booksInCart, result);
          } else {
            return this._storage.set(this.booksInCart, [book]);
          }
        });
      }
    })
  }

  removeFromCart(bookId) {
    this.getBooks().then(result => {
      var itemFound = result.filter(x => x.id == bookId)[0];
      itemFound.isAdded = false;
      this._storage.set(this.listBooks, result);
    })

    return this.checkCart().then(result => {
      if (result) {
        var book = result.filter(x => x.id == bookId)[0],
            index = result.indexOf(book);
        result.splice(index, 1);
        return this._storage.set(this.booksInCart, result);
      }
    });
  }

  isAdded(bookId) {
    return this.checkCart().then(result => {
      if (result) {
        var book = result.filter(x => x.id == bookId)[0];
        return result.indexOf(book) !== -1;
      } else {
        return false;
      }
    });
  }

  createBooks(): IBookInfo[] {
    var books = [
      {
        "id": 1,
        "title": "Tony Buổi Sáng - Trên Đường Băng",
        "description": "Trên đường băng là tập hợp những bài viết được ưa thích trên Facebook của Tony Buổi Sáng. Nhưng khác với một tập tản văn thông thường, nội dung các bài được chọn lọc có chủ đích, nhằm chuẩn bị về tinh thần, kiến thức…cho các bạn trẻ vào đời. ",
        "imgUrl": "assets/imgs/books/tony-tren-duong-bang.jpg",
        "isAdded": false
      },
      {
        "id": 2,
        "title": "Tony Buổi Sáng - Trên Đường Băng",
        "description": "Trên đường băng là tập hợp những bài viết được ưa thích trên Facebook của Tony Buổi Sáng. Nhưng khác với một tập tản văn thông thường, nội dung các bài được chọn lọc có chủ đích, nhằm chuẩn bị về tinh thần, kiến thức…cho các bạn trẻ vào đời. ",
        "imgUrl": "assets/imgs/books/tony-tren-duong-bang.jpg",
        "isAdded": false
      },
      {
        "id": 3,
        "title": "Tony Buổi Sáng - Trên Đường Băng",
        "description": "Trên đường băng là tập hợp những bài viết được ưa thích trên Facebook của Tony Buổi Sáng. Nhưng khác với một tập tản văn thông thường, nội dung các bài được chọn lọc có chủ đích, nhằm chuẩn bị về tinh thần, kiến thức…cho các bạn trẻ vào đời. ",
        "imgUrl": "assets/imgs/books/tony-tren-duong-bang.jpg",
        "isAdded": false
      },
      {
        "id": 4,
        "title": "Tony Buổi Sáng - Trên Đường Băng",
        "description": "Trên đường băng là tập hợp những bài viết được ưa thích trên Facebook của Tony Buổi Sáng. Nhưng khác với một tập tản văn thông thường, nội dung các bài được chọn lọc có chủ đích, nhằm chuẩn bị về tinh thần, kiến thức…cho các bạn trẻ vào đời. ",
        "imgUrl": "assets/imgs/books/tony-tren-duong-bang.jpg",
        "isAdded": false
      }
    ];
    return books;
  }

  getBooks() {
    return this._storage.get(this.listBooks).then(result => {
      if (result) {
        return result;
      } else {
        var books = this.createBooks();
        return this._storage.set(this.listBooks, books);
      } 
    })
  }
}