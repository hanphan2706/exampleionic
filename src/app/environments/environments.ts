export const Environment = {
   production: false,
   firebase: {
      apiKey: "AIzaSyC6jPBBVCxqTIIbNwDUmwYQJmqEdNdx4Kc",
      authDomain: "exampleionic-1b116.firebaseapp.com",
      databaseURL: "https://exampleionic-1b116.firebaseio.com",
      projectId: "exampleionic-1b116",
      storageBucket: "exampleionic-1b116.appspot.com",
      messagingSenderId: "33673865754"
   }
};